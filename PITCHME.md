
# Project Name
<br>
## "Instrumentation"

---

### Schema Characteristics
<br>
* Easy to read; Preferably in JSON
* Componentized
* Supports Inheritance
* Supports Evolution; Inherent Version Support
* Strong Typed
* Tool Support
* Big Data Support
* Matured; Community Support
* Support for custom domain fields
---

### Contenders
<br>
* XML
* JSON Schema
* Protobuf
* Apache Thrift
* Apache Avro

---

### Winner
<br>
![Avro](http://avro.apache.org/images/avro-logo.png)

---

### Advantages

---

### Schema and IDL Support

```
record TestRecord {
    @order("ignore")
    string name;

    @order("descending")
    Kind kind;

    MD5 hash;

    union { MD5, null} @aliases(["hash"]) nullableHash;

    array<long> arrayOfLongs;
  }

 ```

---

### Schema is in JSON

 ```json
 {
  "type": "record",
  "name": "HandshakeRequest", "namespace":"org.apache.avro.ipc",
  "fields": [
    {"name": "clientHash",
     "type": {"type": "fixed", "name": "MD5", "size": 16}},
    {"name": "clientProtocol", "type": ["null", "string"]},
    {"name": "serverHash", "type": "MD5"},
    {"name": "meta", "type": ["null", {"type": "map", "values": "bytes"}]}
  ]
}
```

---

### Schema Type - Primitives
<br>
* null
* boolean
* int
* long
* float
* double
* bytes
* string

---

### Schema Type - Complex - Record
<br>
```json
{
  "type": "record",
  "name": "Powerform",
  "aliases": ["PF"],                      
  "fields" : [
    {"name": "section", "type": "string"}
  ]
}
```

---

### Schema Type - Complex - Enum
<br>
```json
{ "type": "enum",
  "name": "Products",
  "symbols" : ["BTF", "ACP", "AMBER", "DVA"]
}
```

---

### Schema Type - Complex - Arrays
<br>
```json
{"type": "array", "items": "string"}
```

---

### Schema Type - Complex - Maps
<br>
```json
{"type": "map", "values": "long"}
```

---

### Schema Type - Complex - Union
<br>
```json
["null", "string"]
```

---

### Schema Type - Complex - Fixed
<br>
```json
{"type": "fixed", "size": 16, "name": "md5"}
```

---

### Namespaces and Aliases
<br>
```json
{
  "type": "record",
  "name": "Child Obesity Powerform",
  "namespace": "sesi",
  "aliases": ["Child Obs PF", "nsprd.Child Obesity PF", "sbb.dev.CO PF", "sbb.gold.Child PF"],                      
  "fields" : [
    ...
  ]
}
```

---

### Schema Evolution
<br>
* Writer's Schema vs. Reader's Schema
* RESTFul Schema Registry (using MongoDB)
* Instance links to Entity's Schema

---

### Serialization
<br>
* Binary format; small size; faster
* Object Container File = Writer's Schema + Data

---

### Entity and Instance

<div>

<img src='https://g.gravizo.com/svg?
@startuml;
skinparam monochrome false;

class Entity;
object Instance;

Entity <|--down-- Instance;

@enduml
'>

</div>

---

### Every Entity Must have Schema

<div>

<img src='https://g.gravizo.com/svg?
@startuml;
skinparam monochrome false;

interface Schema;
class Entity;
object Instance;

Schema <|--left- Entity;
Entity <|--down-- Instance;

@enduml
'>

</div>

---

### Step 1: Entity Identification
<br>
* Identifying the base Entities in play
* This can be done according to levels defined.

---

### Step 2: Relating Entities
<br>
* Two types of relationships

---

### Inherited Relationship

<div>

<img src='https://g.gravizo.com/svg?
@startuml;
skinparam monochrome false;

class parent;
class child;
class powerform;
class dva_powerform;
class section;
class service_event_section;

parent <|--down-- child;
powerform <|--down-- dva_powerform;
section <|--down-- service_event_section;

@enduml
'>

</div>

---

### Composed Relationship

<div>

<img src='https://g.gravizo.com/svg?
@startuml;
skinparam monochrome false;

class composer;
class component;
class powerform;
class section;
class dva_powerform;
class service_event_section;

composer o-- component;
powerform o-- section;
dva_powerform o-- service_event_section;

@enduml
'>

</div>

---

### Entity Types

<div>

<img src='https://g.gravizo.com/svg?
@startuml;
skinparam monochrome false;

class Natural;
class Synthetic;

@enduml
'>

</div>

---

### Step 2: Relating Entities - Progressive Extension
<br>
* Synthetic Entities to be evolved organically
* Supports Agile product development
---

### Step 3: Entity Collation
<br>
* Collating Aliases at all Entity Levels
* Manual Effort
* Can be gradual

---

### Step 4: Instance Instrumentation
<br>
* Mining for Instances based on Entities
* Supports advanced instrumentation - usage, compliance etc.

---

### Name, Namespace and Aliases
<br>
* Name must be unique per Namespace
* Namespace to represent domains
* Cross domain aliases needs to be namespaced

---

### Software Components

<div>

<img src='https://g.gravizo.com/svg?
@startuml;
skinparam monochrome false;

database MongoDB{;
database "Schema Registry";
database "Instance Storage";
};

node "Natural Entity Creator";
node "Schema Collator";
node "Instance Miner";
node "Instrumenter";

@enduml
'>

</div>


















